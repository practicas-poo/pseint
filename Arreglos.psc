Proceso Arreglos
	Definir arreglo Como Entero;
	Definir indice Como Entero;
	Dimension arreglo[5];
	
	arreglo[0] = 100;
	arreglo[1] = 11;
	arreglo[2] = -12;
	arreglo[3] = 136;
	arreglo[4] = 15;
	
	Para indice=0 Hasta 4 Con Paso 1 Hacer
		Escribir arreglo[indice];
	FinPara
FinProceso