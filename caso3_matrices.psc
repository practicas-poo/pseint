Proceso Caso3
	Definir matriz Como Entero;
	Definir fila Como Entero;
	Definir columna Como Entero;
	Definir vector Como Entero;
	Definir indice Como Entero;
	Dimension matriz[5,5];
	Dimension  vector[25];
	
	
	Para fila<-0 Hasta 4 Con Paso 1 Hacer
		Para columna<-0 Hasta 4 Con Paso 1 Hacer
			matriz[fila,columna]=azar(100);
		FinPara
	FinPara
	Para fila<-0 Hasta 4 Con Paso 1 Hacer
		Para columna<-0 Hasta 4 Con Paso 1 Hacer
		   Escribir Sin Saltar matriz[fila,columna]," ";
	   FinPara
	   Escribir "";
	FinPara
	
	indice = 0;
	Para fila<-0 Hasta 4 Con Paso 1 Hacer
		Para columna<-0 Hasta 4 Con Paso 1 Hacer
			Si matriz[fila,columna]%2==0 Entonces
				vector[indice]=matriz[fila,columna];
				indice = indice +1;
		    FinSi
		FinPara
	FinPara
	indice = indice -1;
	Para fila<-0 Hasta indice Con Paso 1 Hacer
		Escribir "Vector:",vector[fila];
	FinPara
FinProceso
