Proceso PruebaPDAAreglos
	Definir opcion Como Entero;
	
	Repetir
		Limpiar Pantalla;
		
		//Genero Men�
		Escribir "==== MENU DE OPCIONES ====";
		Escribir "1.- Imprimir sumatoria de datos de un arreglo de largo (15)";
		Escribir "2.- Imprimir la diagonal principal de un matriz de 3x3 ";
		Escribir "3.- Imprimir la sumatoria de una matriz de 4x3 ";
		Escribir "4.- Salir";
		Escribir Sin Saltar "Seleccione una opci�n: ";
		Leer opcion;
		
		//genero procesos segun opci�n
		Segun opcion Hacer
			1:				
				arreglo1();
			2:
				arreglo2();
			3:
				arreglo3();
			4:
				Escribir "ADIOS-----";
			De Otro Modo:
				Escribir "Opci�n Incorrecta";
		FinSegun
	Hasta Que opcion == 4
FinProceso


SubProceso arreglo1
	definir vector1 como Entero;
	Dimension vector1[15];
	Definir indice como Entero;
	
	Definir sumatoria1 Como Entero;
	sumatoria1 = 0;
	
	Para indice<-0 Hasta 14 Con Paso 1 Hacer
		vector1[indice] <- Azar(100);
	FinPara
	
	Limpiar Pantalla;
	
	Para indice<-0 Hasta 14 Con Paso 1 Hacer
		sumatoria1 = sumatoria1 + vector1[indice];
	FinPara
	
	Escribir "La sumatoria de los 15 numeros es: ",sumatoria1;
FinSubProceso

SubProceso arreglo2
	Definir filas Como Entero;
	Definir columnas Como Entero;
	definir vector2 como entero;
	Dimension vector2[3,3];
	
	Para filas=0 hasta 2 con paso 1 hacer
		Para columnas=0 hasta 2 con paso 1 hacer 
			vector2[filas,columnas] = Azar(100);
		FinPara
	FinPara
	
	Para filas=0 hasta 2 con paso 1 hacer
		Para columnas=0 hasta 2 con paso 1 hacer 
			si (filas == columnas) entonces
				Escribir vector2[filas,columnas];
			FinSi
		FinPara
	FinPara
FinSubProceso

SubProceso arreglo3
	Definir filas Como Entero;
	Definir columnas Como Entero;
	definir vector3 como entero;
	Dimension vector3[4,3];
	Definir sumatoria2 Como Entero;
	sumatoria2 = 0;
	
	Para filas=0 hasta 2 con paso 1 hacer
		Para columnas=0 hasta 2 con paso 1 hacer 
			vector3[filas,columnas] = Azar(100);
		FinPara
	FinPara
	
	Para filas=0 hasta 2 con paso 1 hacer
		Para columnas=0 hasta 2 con paso 1 hacer 
			sumatoria2 = sumatoria2 + vector3[filas,columnas];
		FinPara
	FinPara
	
	Escribir "La sumatoria de la matriz(4,3) es: ",sumatoria2;
FinSubProceso
	