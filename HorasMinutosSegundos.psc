Proceso HorasMinutosSegundos
	Definir hora Como Entero;
	Definir minutos Como Entero;
	definir segundos Como Entero;
	
	Escribir "Ingrese la hora: ";
	Leer hora;
	
	Escribir "Ingrese los minutos: ";
	Leer minutos;
	
	Escribir "Ingrese los segundos: ";
	Leer segundos;
	
	si ((hora > 0 && hora < 24) && (minutos > 0 && minutos < 60) && (segundos > 0 && segundos < 60))	Entonces
		Escribir "La hora ingresada es: ",hora,":",minutos,":",segundos;
	Sino
		Escribir "La hora ingresada no es correcta";
	FinSi
FinProceso