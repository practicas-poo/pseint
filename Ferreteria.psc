Proceso Ferreteria
	//defino variables articulos, precio y descuento
	Definir articulo1 Como Caracter;
	Definir articulo2 Como Caracter;
	Definir articulo3 Como Caracter;
	Definir precioArticulo1 Como Entero;
	Definir precioArticulo2 Como Entero;
	Definir precioArticulo3 Como Entero;
	Definir precio como Entero;
	Definir precioFinal Como Entero;
	Definir descuento Como Entero;
	
	//Solicito productos y precios al usuario.
	Escribir Sin Saltar "Ingrese Articulo 1: ";
	Leer articulo1;
	Escribir Sin Saltar "Ingrese precio del Articulo 1: ";
	Leer precioArticulo1;
	Escribir Sin Saltar "Ingrese Articulo 2: ";
	Leer articulo2;
	Escribir Sin Saltar "Ingrese precio del Articulo 2: ";
	Leer precioArticulo2;
	Escribir Sin Saltar "Ingrese Articulo 1: ";
	Leer articulo3;
	Escribir Sin Saltar "Ingrese precio del Articulo 1: ";
	Leer precioArticulo3;
	
	precio = precioArticulo1 + precioArticulo2 + precioArticulo3;
	
	Si precio > 50000 Entonces
		//descuento es de 15%
		descuento = (precio * 15) / 100;
		precioFinal = precio - descuento;
		Escribir "Total venta: ",precio;
		Escribir "Felicidades, Tiene un descuento de 15%";
		Escribir "El valor total de su compra es: ",precioFinal;
	Sino
		Escribir "Total venta: ",precio;
		Escribir "No tiene descuento..";
		Escribir "El valor total de su compra es: ",precio;
	FinSi
	
FinProceso