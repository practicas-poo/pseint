Proceso Palindromo
	//Ingrsar un numero de tres digitos
	//indicar i es palindromo o no
	Definir valor Como Entero;
	Definir unidad Como Entero;
	Definir decena Como Entero;
	Definir resultado Como Entero;
	
	Escribir Sin Saltar "Ingrese n�mero entero:";
	Leer valor;
	
	unidad = valor%10;
	resultado = valor/10;
	decena = resultado%10;
	resultado = resultado/10;
	centena = resultado%10;
	
	Si (unidad==centena) entonces
		Escribir "Es palindromo";
	Sino
		Escribir "No es palindromo";
	FinSi
FinProceso