Proceso Potencia
	Definir valor Como Entero;
	Definir resultado Como Entero;
	
	Escribir Sin Saltar "Ingrese un numero:";
	Leer valor;
	
	Si (valor > 0 & valor <=10) Entonces
		resultado = valor^2;
		Escribir "El resultado es: ",resultado;
	Sino
		Si (valor > 10 & valor <=20) Entonces
			resultado = valor^3;
			Escribir "El resultado es: ",resultado;
		Sino
			Escribir "Fuera de rango";
		FinSi
	FinSi
FinProceso