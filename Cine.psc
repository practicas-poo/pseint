Proceso Cine
	Definir edad Como Entero;
	definir actividad Como Caracter;
	Definir sexo Como Caracter;
	Definir limite Como Caracter;
	Definir cantMujeres como entero;
	definir cantEstudiantesVarones Como Entero;
	Definir totalEdad como Entero;
	definir promedio como Real;
	
	cantMujeres = 0;
	cantEstudiantesVarones = 0;
	edad = 0;
	totalEdad = 0;
	
	Repetir
		Escribir Sin Saltar "Edad: ";
		Leer edad;
		
		si ((edad >= 1) & (edad <=100)) Entonces
			Escribir Sin Saltar "Actividad (estudiante,empleado)?: ";
			Leer actividad;
			
			si ((actividad == "estudiante") | (actividad == "empleado")) Entonces
				Escribir Sin Saltar "Sexo (F,M): ";
				Leer sexo;
				
				si((sexo == 'F') | (sexo == 'M')) Entonces
					si((sexo == 'F') & edad >= 60) Entonces
						cantMujeres = cantMujeres + 1;
					FinSi
					
					si((sexo == 'M') & (actividad == "estudiante")) Entonces
						cantEstudiantesVarones = cantEstudiantesVarones + 1;
						totalEdad = totalEdad + edad;
					FinSi
				Sino
					Escribir "Sexo incorrecto (F o M)";
				FinSi
			Sino
				Escribir "Actividad INCORRECTA";
			FinSi
		Sino
			Escribir "Edad incorrecta, intente (1...100)";
		FinSi
		
		Escribir "Desea seguir encuestando (S,N): ";
		leer limite;
	Hasta Que (limite == 'N');
	
	Escribir "La cantidad de Mujeres tercera edad es: ", cantMujeres;
	
	si (cantEstudiantesVarones == 0) entonces
		Escribir "No hay registro de estudiantes varones..";
	Sino
		promedio = totalEdad / cantEstudiantesVarones;
		Escribir "El promedio de edad de los estudiantes varones es: ", promedio;
	FinSi
	
FinProceso