Proceso Edades
	//defino variables
	Definir persona1 Como Caracter;
	Definir persona2 Como Caracter;
	Definir persona3 Como Caracter;
	Definir persona4 Como Caracter;
	Definir edadPersona1 Como Entero;
	Definir edadPersona2 Como Entero;
	Definir edadPersona3 Como Entero;
	Definir edadPersona4 Como Entero;
	definir mediaEdades Como Real;
	
	//solicito nombres y edades de las personas
	Escribir Sin Saltar "Ingrese nombre de persona 1: ";
	Leer persona1;
	Escribir Sin Saltar "Ingrese edad de persona 1: ";
	Leer edadPersona1;
	Escribir Sin Saltar "Ingrese nombre de persona 2: ";
	Leer persona2;
	Escribir Sin Saltar "Ingrese edad de persona 2: ";
	Leer edadPersona2;
	Escribir Sin Saltar "Ingrese nombre de persona 3: ";
	Leer persona3;
	Escribir Sin Saltar "Ingrese edad de persona 3: ";
	Leer edadPersona3;
	Escribir Sin Saltar "Ingrese nombre de persona 4: ";
	Leer persona4;
	Escribir Sin Saltar "Ingrese edad de persona 4: ";
	Leer edadPersona4;
	
	//calculo la media de las edades
	mediaEdades = (edadPersona1+edadPersona2+edadPersona3+edadPersona4)/4;
	Escribir "La media de las edades es: ",mediaEdades;
	
	//obtengo persona mayor
	Si ((edadPersona1 > edadPersona2) & (edadPersona1 > edadPersona3) & (edadPersona1 > edadPersona4)) Entonces
		Escribir "La persona mayor es: ",persona1;
		Escribir "Su edad es: ",edadPersona1;
	FinSi
	
	Si ((edadPersona2 > edadPersona1) & (edadPersona2 > edadPersona3) & (edadPersona2 > edadPersona4)) Entonces
		Escribir "La persona mayor es: ",persona2;
		Escribir "Su edad es: ",edadPersona2;
	FinSi
	
	Si ((edadPersona3 > edadPersona1) & (edadPersona3 > edadPersona2) & (edadPersona3 > edadPersona4)) Entonces
		Escribir "La persona mayor es: ",persona3;
		Escribir "Su edad es: ",edadPersona3;
	FinSi
	
	Si ((edadPersona4 > edadPersona1) & (edadPersona4 > edadPersona2) & (edadPersona4 > edadPersona3)) Entonces
		Escribir "La persona mayor es: ",persona4;
		Escribir "Su edad es: ",edadPersona4;
	FinSi
	
	//obtengo persona menor
	Si ((edadPersona1 < edadPersona2) & (edadPersona1 < edadPersona3) & (edadPersona1 < edadPersona4)) Entonces
		Escribir "La persona menor es: ",persona1;
		Escribir "Su edad es: ",edadPersona1;
	FinSi
	
	Si ((edadPersona2 < edadPersona1) & (edadPersona2 < edadPersona3) & (edadPersona2 < edadPersona4)) Entonces
		Escribir "La persona menor es: ",persona2;
		Escribir "Su edad es: ",edadPersona2;
	FinSi
	
	Si ((edadPersona3 < edadPersona1) & (edadPersona3 < edadPersona2) & (edadPersona3 < edadPersona4)) Entonces
		Escribir "La persona menor es: ",persona3;
		Escribir "Su edad es: ",edadPersona3;
	FinSi
	
	Si ((edadPersona4 < edadPersona1) & (edadPersona4 < edadPersona2) & (edadPersona4 < edadPersona3)) Entonces
		Escribir "La persona menor es: ",persona4;
		Escribir "Su edad es: ",edadPersona4;
	FinSi
FinProceso