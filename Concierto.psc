Proceso Concierto
	Definir valorCancha Como Entero;
	Definir valorGaleria Como Entero;
	Definir total Como Entero;
	Definir cantidadEntradas Como Entero;
	Definir localizacion como Caracter;
	
	valorCancha  = 30000;
	valorGaleria = 18000;
	
	Escribir sin saltar "Cuantas Entradas desea Comprar: ";
	Leer cantidadEntradas;
	
	Si ((cantidadEntradas > 0) & (cantidadEntradas <= 5)) Entonces
		Escribir "CANCHA o GALERIA?: ";
		Leer localizacion;
		
		Si ((localizacion == "CANCHA") | (localizacion == "cancha")) Entonces
			si (cantidadEntradas == 1) Entonces
				Escribir "Valor de la Entrada es: ", valorCancha;
			FinSi
			
			si ((cantidadEntradas == 2) | (cantidadEntradas == 3)) Entonces
				valorCancha = valorCancha - (valorCancha * 0.10);
				Escribir "Valor de la Entrada es: ", valorCancha;
			FinSi
			
			si (cantidadEntradas == 4) Entonces
				valorCancha = valorCancha - (valorCancha * 0.15);
				Escribir "Valor de la Entrada es: ", valorCancha;
			FinSi
			
			si (cantidadEntradas == 5) Entonces
				valorCancha = valorCancha - (valorCancha * 0.20);
				Escribir "Valor de la Entrada es: ", valorCancha;
			FinSi
		Sino
			si (cantidadEntradas == 1) Entonces
				Escribir "Valor de la Entrada es: ", valorGaleria;
			FinSi
			
			si ((cantidadEntradas == 2) | (cantidadEntradas == 3)) Entonces
				valorGaleria = valorGaleria - (valorGaleria * 0.10);
				Escribir "Valor de la Entrada es: ", valorGaleria;
			FinSi
			
			si (cantidadEntradas == 4) Entonces
				valorGaleria = valorGaleria - (valorGaleria * 0.15);
				Escribir "Valor de la Entrada es: ", valorGaleria;
			FinSi
			
			si (cantidadEntradas == 5) Entonces
				valorGaleria = valorGaleria - (valorGaleria * 0.20);
				Escribir "Valor de la Entrada es: ", valorGaleria;
			FinSi
		FinSi
	Sino
		Escribir "Solo puedes comprar de 1 a 5 entradas";
	FinSi
	
FinProceso