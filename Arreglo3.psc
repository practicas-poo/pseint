Proceso Arreglo3
	//EJERCICIO: declarar un arreglo de largo 10
	//llenar y luego imprimir solo los numeros pares guardados
	Definir arreglo Como Entero;
	Definir indice Como Entero;
	Dimension arreglo[10];
	
	Para indice<-0 Hasta 9 Con Paso 1 Hacer
		arreglo[indice] = Azar(1000);
	FinPara
	
	Escribir "Los pares son: ";
	Para indice<-0 Hasta 9 Con Paso 1 Hacer
		Si (arreglo[indice] % 2 == 0) Entonces
			Escribir arreglo[indice];
		FinSi
	FinPara
	
	Escribir "==== ====";
	Para indice<-0 Hasta 9 Con Paso 1 Hacer
		Si (arreglo[indice]<0) Entonces
			Escribir "Negativos->",arreglo[indice];
		FinSi
	FinPara
	
	Escribir "==== ====";
	Escribir "Primero: ",arreglo[0]," y el �ltimo: ",arreglo[9];
	
FinProceso