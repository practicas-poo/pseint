Proceso Menu
	Definir opcion Como Entero;
	definir num1 Como Entero;
	Definir num2 Como Entero;
	Definir resultado Como Entero;
	
	Escribir "==== MENU ====";
	Escribir "1.- Sumar";
	Escribir "2.- Restar";
	Escribir "3.- Multiplicar";
	Escribir "4.- Dividir";
	Escribir "5.- Exponenciar";
	Escribir "6.- Salir";
	Escribir Sin Saltar "Seleccione una opci�n: ";
	Leer opcion;
	
	Segun opcion Hacer
		1:
			Escribir Sin Saltar "Ingrese el primer numero: ";
			Leer num1;
			Escribir Sin Saltar "Ingrese el segundo numero: ";
			Leer num2;
			resultado = num1 + num2;
			Escribir "El resultado de la suma es: ",resultado;
		2:
			Escribir Sin Saltar "Ingrese el primer numero: ";
			Leer num1;
			Escribir Sin Saltar "Ingrese el segundo numero: ";
			Leer num2;
			resultado = num1 - num2;
			Escribir "El resultado de la resta es: ",resultado;
		3:
			Escribir Sin Saltar "Ingrese el primer numero: ";
			Leer num1;
			Escribir Sin Saltar "Ingrese el segundo numero: ";
			Leer num2;
			resultado = num1 * num2;
			Escribir "El resultado de la multiplicacion es: ",resultado;
		4:
			Escribir Sin Saltar "Ingrese el primer numero: ";
			Leer num1;
			Escribir Sin Saltar "Ingrese el segundo numero: ";
			Leer num2;			
			resultado = num1 / num2;
			Escribir "El resultado de la devision es: ",resultado;
		5:
			Escribir Sin Saltar "Ingrese el numero base: ";
			Leer num1;
			Escribir Sin Saltar "Ingrese el exponente: ";
			Leer num2;
			resultado = num1^num2;
			Escribir "El resultado de la potencia es: ",resultado;
		6:
			Escribir Sin Saltar "ADIOS------";
		De Otro Modo:
			Escribir "La opci�n ingresada no es correcta...";
	FinSegun
FinProceso