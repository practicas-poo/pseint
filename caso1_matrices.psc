Proceso sin_titulo
	Definir matriz Como Entero;
	Definir fila Como Entero;
	Definir columna Como Entero;
	Dimension matriz[7,7];
	
	
	Para fila<-0 Hasta 6 Con Paso 1 Hacer
		Para columna<-0 Hasta 6 Con Paso 1 Hacer
			matriz[fila,columna]=azar(201);
		FinPara
	FinPara
	Para fila<-0 Hasta 6 Con Paso 1 Hacer
		Para columna<-0 Hasta 6 Con Paso 1 Hacer
			Escribir Sin Saltar matriz[fila,columna],"  ";
		FinPara
		Escribir " ";
	FinPara
	Escribir "- Primera columna -";
	Para fila<-0 Hasta 6 Con Paso 1 Hacer
		Escribir matriz[fila,0];
	FinPara
	Escribir "- Diagonal Principal -";
	Para fila<-0 Hasta 6 Con Paso 1 Hacer
	Escribir matriz[fila,fila];
	FinPara
	
	Escribir "- Diagonal Secundaria -";
	columna=6;
	Para fila<-0 Hasta 6 Con Paso 1 Hacer
		Escribir matriz[fila,columna];
		columna = columna - 1;
	FinPara
FinProceso
