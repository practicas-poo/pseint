Proceso Vertical
	//	ingresar e imprimir una palabra en formato titulo
	// (primer caracter con mayuscula, el resto minuscula) verticalmente.
	definir palabra Como Caracter;
	definir limite Como Entero;
	definir contador Como Entero;
	definir tamanio Como Entero;
	definir largo como entero;
	definir sub como caracter;
	
	Escribir Sin Saltar "Ingrese cantidad de palabras: ";
	leer limite;
	
	Para contador=1 Hasta limite Con Paso 1 Hacer
		Escribir Sin Saltar "Ingrese palabra",contador,": ";
		leer palabra;
		
		tamanio = Longitud(palabra);
		
		Para largo=0 Hasta tamanio Con Paso 1 Hacer
			sub = SubCadena(palabra,largo,largo);
			Si largo=0 Entonces
				Escribir Mayusculas(sub);
			Sino
				Escribir Minusculas(sub);
			FinSi
			
		FinPara
	FinPara
	
FinProceso